/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repaso;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 *
 * @author lliurex
 */
public class puntosComa {
    public static void main(String[] args) {
        
        String frase="pepe;ana;jorge;maria;manuel;";
        LinkedList<String> nombres=new LinkedList<>();
        
        // Con StringTokenizer e Iterator
        StringTokenizer st=new StringTokenizer(frase);
        while (st.hasMoreTokens()) {
            nombres.add(st.nextToken(";"));
        }
        Iterator it=nombres.iterator();
        while (it.hasNext()) {
            System.out.println((String)it.next());
        }
        
        // Con cadenas y bucles normales
        int inicio=0, fin=0;
        boolean salir=false;
        String palabra;
        while (!salir) {
            fin=frase.indexOf(";", fin);
            if (fin!=-1) {
                palabra=frase.substring(inicio, fin);
                nombres.add(palabra);
                inicio=fin+1;
                fin++;
                
            } else {
                salir=true;
            }
        }
        
        for (String nombre:nombres) {
            System.out.println(nombre);
        }
    }
}
